#ifndef MODELVIEWER_MVSCENE_HPP
#define MODELVIEWER_MVSCENE_HPP
#include <ktnEngine/ktnApplication>

namespace ktn::Application {
class mvScene : public ktnScene {
public:
    class mvSceneInputProcessor : public IktnInputProcessor {
    public:
        mvSceneInputProcessor(GLFWwindow *eWindow) {
            m_Window = eWindow;
            m_InputMapper.Initialize();
        }
        void ProcessInput() {
            if (glfwGetKey(m_Window, m_InputMapper.Map[KTNKEY_ESCAPE]) == GLFW_PRESS) { glfwSetWindowShouldClose(m_Window, 1); }

            if (glfwGetKey(m_Window, m_InputMapper.Map[KTNKEY_W]) == GLFW_PRESS) { KeyPressed.Emit(KTNKEY_W); }
            if (glfwGetKey(m_Window, m_InputMapper.Map[KTNKEY_A]) == GLFW_PRESS) { KeyPressed.Emit(KTNKEY_A); }
            if (glfwGetKey(m_Window, m_InputMapper.Map[KTNKEY_S]) == GLFW_PRESS) { KeyPressed.Emit(KTNKEY_S); }
            if (glfwGetKey(m_Window, m_InputMapper.Map[KTNKEY_D]) == GLFW_PRESS) { KeyPressed.Emit(KTNKEY_D); }
            if (glfwGetKey(m_Window, m_InputMapper.Map[KTNKEY_UP]) == GLFW_PRESS) { KeyPressed.Emit(KTNKEY_UP); }
            if (glfwGetKey(m_Window, m_InputMapper.Map[KTNKEY_DOWN]) == GLFW_PRESS) { KeyPressed.Emit(KTNKEY_DOWN); }
            if (glfwGetKey(m_Window, m_InputMapper.Map[KTNKEY_LEFT]) == GLFW_PRESS) { KeyPressed.Emit(KTNKEY_LEFT); }
            if (glfwGetKey(m_Window, m_InputMapper.Map[KTNKEY_RIGHT]) == GLFW_PRESS) { KeyPressed.Emit(KTNKEY_RIGHT); }
            if (glfwGetKey(m_Window, m_InputMapper.Map[KTNKEY_SPACE]) == GLFW_PRESS) { KeyPressed.Emit(KTNKEY_SPACE); }
            if (glfwGetKey(m_Window, m_InputMapper.Map[KTNKEY_SPACE]) == GLFW_RELEASE) { KeyReleased.Emit(KTNKEY_SPACE); }
            if (glfwGetKey(m_Window, m_InputMapper.Map[KTNKEY_LEFT_CONTROL]) == GLFW_PRESS) { KeyPressed.Emit(KTNKEY_LEFT_CONTROL); }
        }

        ktnSignal<ktnKey> KeyPressed;
        ktnSignal<ktnKey> KeyReleased;

    private:
        GLFWwindow *m_Window;

        ktnInputMapper m_InputMapper;
    };
    mvScene( //
        Graphics::ktnGraphicsScenePtr eGraphicsScene,
        Physics::ktnPhysicsScenePtr ePhysicsScene,
        std::shared_ptr<mvSceneInputProcessor> eInputProcessor);
    void ProcessInput() final;

    ktnSignal<std::string> StartAction;
    ktnSignal<> SpaceIsPressed;
    ktnSignal<> CtrlIsPressed;

protected:
    void HandleEvent_ItemsDroppedIntoWindow(int count, const char **paths) final;
    void HandleEvent_MouseClicked(int button, int action, int mods) final;
    void HandleEvent_MouseMoved(double xpos, double ypos) final;

private:
    void HandleEvent_KeyPressed(ktnKey eKey);
    void HandleEvent_KeyReleased(ktnKey eKey);

    void HandleCtrlPress();
    void HandleSpacePress();

    void IncrementJointNr();
    void DecrementJointNr();
    std::unordered_map<ktnKey, bool> m_PressedKeys;
    int _currentJoint = 0;

    bool m_LeftMouseDragged = false;
};
} // namespace ktn::Application
#endif // MODELVIEWER_MVSCENE_HPP
