#include "mvScene.hpp"

using namespace glm;
using namespace ktn;
using namespace ktn::Graphics;
using namespace ktn::Physics;
using namespace std;

namespace ktn::Application {
mvScene::mvScene( //
    ktnGraphicsScenePtr eGraphicsScene,
    ktnPhysicsScenePtr ePhysicsScene,
    shared_ptr<mvSceneInputProcessor> eInputProcessor)
    : ktnScene(eGraphicsScene, ePhysicsScene) {

    auto camera = make_shared<ktnCamera>();
    camera->SetType(CameraType::FIRSTPERSON);
    camera->SetPosition({-2.f, 1.0f, 2.0f}, WorldSpace);
    camera->TurnLeft(450);
    camera->TurnDown(150);
    camera->SetMode(CameraMode::FREELOOK);

    GraphicsScene()->SetCurrentCamera(camera);

    srand(static_cast<uint32_t>(std::chrono::steady_clock::now().time_since_epoch().count()));

    auto light = make_shared<ktnLight>(string("light").c_str());

    light->Intensity = 1000;
    light->Falloff.LinearTerm = 0.5f;
    light->Falloff.QuadraticTerm = 0.5f;
    light->SetPosition(GraphicsScene()->CurrentCamera()->Position(WorldSpace) + GraphicsScene()->CurrentCamera()->Front() * 0.1f, WorldSpace);
    light->SetDirection(GraphicsScene()->CurrentCamera()->Front());
    GraphicsScene()->AddLight(light);

    eInputProcessor->KeyPressed.Connect(this, &mvScene::HandleEvent_KeyPressed);
    eInputProcessor->KeyReleased.Connect(this, &mvScene::HandleEvent_KeyReleased);
    InputProcessor = eInputProcessor;
}

void mvScene::ProcessInput() {
    auto currentFrameTime = static_cast<float>(glfwGetTime());
    m_DeltaTime = currentFrameTime - m_LastFrameTime;
    m_LastFrameTime = currentFrameTime;

    InputProcessor->ProcessInput();

    m_GraphicsScene->CurrentCamera()->UpdateViewMatrix();

    if (m_LeftMouseIsClicking) {
        m_GraphicsScene->Lights[0]->SetPosition(
            m_GraphicsScene->CurrentCamera()->Position(WorldSpace) + m_GraphicsScene->CurrentCamera()->Front() * 0.1f, WorldSpace);
        m_GraphicsScene->Lights[0]->SetDirection(m_GraphicsScene->CurrentCamera()->Front());
    }
}

void mvScene::HandleEvent_ItemsDroppedIntoWindow(int count, const char **paths) {
    for (int i = 0; i < count; ++i) {
        try {
            auto model = make_shared<ktnModel>(paths[i]);
            m_GraphicsScene->AddModel(model);
        } catch (std::exception &e) { cout << e.what() << endl; }
    }
}

void mvScene::HandleEvent_KeyPressed(ktnKey eKey) {
    float cameraSpeed = 2.0f * m_DeltaTime;
    switch (eKey) {
    case KTNKEY_W:
        m_GraphicsScene->CurrentCamera()->MoveForward(cameraSpeed);
        break;
    case KTNKEY_A:
        m_GraphicsScene->CurrentCamera()->MoveLeft(cameraSpeed);
        break;
    case KTNKEY_S:
        m_GraphicsScene->CurrentCamera()->MoveBackward(cameraSpeed);
        break;
    case KTNKEY_D:
        m_GraphicsScene->CurrentCamera()->MoveRight(cameraSpeed);
        break;
    case KTNKEY_UP:
        m_GraphicsScene->CurrentCamera()->TurnUp(1);
        m_GraphicsScene->CurrentCamera()->UpdateFront();
        m_GraphicsScene->CurrentCamera()->UpdateViewMatrix();
        break;
    case KTNKEY_DOWN:
        m_GraphicsScene->CurrentCamera()->TurnDown(1);
        m_GraphicsScene->CurrentCamera()->UpdateFront();
        m_GraphicsScene->CurrentCamera()->UpdateViewMatrix();
        break;

    case KTNKEY_LEFT:
        m_GraphicsScene->CurrentCamera()->TurnLeft(1);
        m_GraphicsScene->CurrentCamera()->UpdateFront();
        m_GraphicsScene->CurrentCamera()->UpdateViewMatrix();
        break;
    case KTNKEY_RIGHT:
        m_GraphicsScene->CurrentCamera()->TurnRight(1);
        m_GraphicsScene->CurrentCamera()->UpdateFront();
        m_GraphicsScene->CurrentCamera()->UpdateViewMatrix();
        break;
    case KTNKEY_LEFT_CONTROL:
        HandleCtrlPress();
        break;
    case KTNKEY_SPACE:
        HandleSpacePress();
        break;
    default:
        break;
    }
}

void mvScene::HandleEvent_KeyReleased(ktnKey eKey) {
    switch (eKey) {
    case KTNKEY_SPACE:
        if (m_PressedKeys[KTNKEY_SPACE] == true) {
            GraphicsScene()->Models.at(0)->StopAction("walk");
            m_PressedKeys[KTNKEY_SPACE] = false;
        }
        break;
    default:
        break;
    }
}

void mvScene::HandleEvent_MouseClicked(int button, int action, int mods) {
    // for the time being the modification does nothing, but can be added to do stuff later.
    if (mods != 0) {}

    if (GLFW_MOUSE_BUTTON_LEFT == button) {
        if (action == GLFW_PRESS) { m_LeftMouseIsClicking = true; }
        if (action == GLFW_RELEASE) {
            m_LeftMouseIsClicking = false;
            if (m_LeftMouseDragged) { // select object if mouse was not dragged?
                m_LeftMouseDragged = false;
            }
        }
    }
}

void mvScene::HandleEvent_MouseMoved(double xpos, double ypos) {
    if (m_FirstMouse) {
        m_LastX = xpos;
        m_LastY = ypos;
        m_FirstMouse = false;
    }
    m_GraphicsScene->CurrentCamera()->TurnRight(float(xpos - m_LastX));
    m_GraphicsScene->CurrentCamera()->TurnDown(float(ypos - m_LastY));
    m_GraphicsScene->CurrentCamera()->UpdateFront();
    m_GraphicsScene->CurrentCamera()->UpdateViewMatrix();
    m_LastX = xpos;
    m_LastY = ypos;
}

void mvScene::HandleCtrlPress() {
    GraphicsScene()->Models.at(0)->StartAction("Idle");
}

void mvScene::HandleSpacePress() {
    m_PressedKeys[KTNKEY_SPACE] = true;
    GraphicsScene()->Models.at(0)->StartAction("walk");
}
} // namespace ktn::Application
