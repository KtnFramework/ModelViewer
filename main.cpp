#include <ktnEngine/ktnGraphics>

#include "mvScene.hpp"

#include <glm/gtc/matrix_transform.hpp>

#include <mutex>
#include <random>

using namespace glm;
using namespace ktn;
using namespace ktn::Application;
using namespace ktn::Graphics;
using namespace ktn::Physics;
using namespace std;

int main() {
    // create the application
    auto app = ktnApplication::CreateApplicationWithDefaultRenderer<ktnRenderer_OpenGL_ImGui>();

    // create the window to show the application
    ktnApplicationWindow window(vk::Extent2D(1024, 768));
    window.WindowName = "Keyframe Animation Example";
    window.Initialize(app);
    window.HideCursor();

    // create the scene
    auto scene = make_shared<mvScene>( //
        make_shared<ktnGraphicsScene>(),
        make_shared<ktnPhysicsScene>(),
        make_shared<mvScene::mvSceneInputProcessor>(window.Window));

    // This is particularly leaking memory.
    // If we create normal variables here they will go out of scope after the if
    // We keep it for the time being, as the vulkan backend needs to have the
    // shader and model classes changed as well.
    if (app->Renderer->Backend() == ktnGraphicsBackend::OPENGL) {

        auto model = make_shared<ktnModel>("resources/models/placeholder_robot.gltf");

        scene->StartAction.Connect(model.get(), &ktnModel::StartAction);

        model->Name = "Model";
        scene->GraphicsScene()->AddModel(model);
    }

    app->SetActiveScene(scene);

    // render loop
    window.Run();

    return 0;
}
